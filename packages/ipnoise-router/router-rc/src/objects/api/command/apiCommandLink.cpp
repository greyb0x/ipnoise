/*
 *
 *  *** IPNoise ***
 *
 *  Roman E. Chechnev Oct 2011 (c)
 *  <morik@ipnoise.ru>
 *
 */

#include "apiCommandLink.hpp"

ApiCommandLink::ApiCommandLink(const string &a_name)
    :   ApiCommand(a_name)
{
}

ApiCommandLink::~ApiCommandLink()
{
}

