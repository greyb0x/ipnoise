#ifndef ELEMENTS_HPP
#define ELEMENTS_HPP

// objects
#include "objects/object.h"
#include "objects/ipnoiseObject.h"
#include "objects/client/clientHandler.h"
#include "objects/client/handlerHttp/clientHandlerHttpObject.h"
#include "objects/client/handlerRaw/clientHandlerRawObject.h"
#include "objects/client/handlerTelnet/clientHandlerTelnetObject.h"
#include "objects/client/handlerUnknown/clientHandlerUnknownObject.h"
#include "objects/clientObject.h"
#include "objects/groupItemObject.h"
#include "objects/groupObject.h"
#include "objects/groupsObject.h"
#include "objects/contactsObject.h"
#include "objects/contactObject.h"
#include "objects/contactItemsObject.h"
#include "objects/contactItemObject.h"
#include "objects/itemsObject.h"
#include "objects/itemObject.h"
#include "objects/neighCheckingsObject.h"
#include "objects/neighCheckingObject.h"
#include "objects/neighObject.h"
#include "objects/neighsObject.h"
#include "objects/netClientHttpObject.h"
#include "objects/netClientObject.h"
#include "objects/netCommandObject.h"
#include "objects/netEventObject.h"
#include "objects/serverObject.h"
#include "objects/acceptedClientObject.h"
#include "objects/packetsObject.h"
#include "objects/packetObject.h"
#include "objects/packetReceiversObject.h"
#include "objects/packetReceiverObject.h"

// sessions
#include "objects/sessionsObject.h"
#include "objects/sessionObject.h"
#include "objects/sessionUnknownObject.h"
#include "objects/sessionClientObject.h"
#include "objects/sessionItemObject.h"
#include "objects/sessionIPNoiseObject.h"
#include "objects/sessionLoObject.h"

#include "objects/skBuffObject.h"
#include "objects/linksObject.h"
#include "objects/linkObject.h"
#include "objects/userObject.h"
#include "objects/usersObject.h"
#include "objects/signalsObject.h"
#include "objects/slotsObject.h"

// conferences
#include "objects/conferencesObject.h"
#include "objects/conferenceObject.h"
#include "objects/conferenceItemsObject.h"
#include "objects/conferenceItemObject.h"
#include "objects/conferenceMsgsObject.h"
#include "objects/conferenceMsgObject.h"

#endif

