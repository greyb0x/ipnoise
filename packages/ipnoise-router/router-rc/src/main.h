/*
 *
 *  *** IPNoise ***
 *
 *  Roman E. Chechnev Feb 2010 (c)
 *  <ipnoise@chechnev.ru>
 *
 */

#ifndef MAIN_H
#define MAIN_H

#define IPNOISE_ROUTER_VERSION "0.14"

class DomDocument;

#define DEFAULT_IPNOISE_DIR     "/etc/ipnoise/"
#define DEFAULT_PROFILE_DIR     DEFAULT_IPNOISE_DIR"/profile/"
#define DEFAULT_CONTAINERS_DIR  DEFAULT_PROFILE_DIR"/containers/"
#define DEFAULT_CONFIG_FILE     DEFAULT_PROFILE_DIR"/config.xml"

#endif

