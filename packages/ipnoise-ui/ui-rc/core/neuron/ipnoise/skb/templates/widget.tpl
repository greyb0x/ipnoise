<div style="width: 100%;"
    id="<perl>return getCurNeuronId()</perl>"
    class="neuron neuronIPNoiseSkb">

    <script>
        $(document).ready(function(){
            // setup neuron
            var neuron = NeuronIPNoiseSkbClass();
            neuron.setId(
                "<perl>return getCurNeuronId()</perl>"
            );
            neuron.load();
            neuron.render();
        });
    </script>

    <h1>IPNoiseSkb</h1>
</div>

